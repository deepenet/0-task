#include <iostream>
#include <fstream>
#include <string>
#include <regex>

using wordNumbPair = std::pair<std::string, unsigned int>;

unsigned int analyseStr(std::fstream &Stream, std::map<std::string, unsigned int> &wordsMap) {

	unsigned int numbAllWords = 0;
	std::string inputStr;
	std::regex rgx("[^[:w:]]+");

	while (std::getline(Stream, inputStr)) {
		std::sregex_token_iterator iter(inputStr.begin(), inputStr.end(), rgx, -1); 
		std::sregex_token_iterator end;
		for (; iter != end; ++iter) { 
			auto ptr = wordsMap.find(*iter);
			if(*iter != "") {
				wordsMap[*iter]++;
				numbAllWords++;
			}
		}
	}

	return numbAllWords;
}

void fillWordsOutMap(std::multimap<unsigned int, std::string> &wordsOutMap, std::map<std::string, unsigned int> &wordsWorkMap) {
	for(auto i = wordsWorkMap.begin(); i != wordsWorkMap.end(); ++i) {
		wordsOutMap.emplace(i->second, i->first);
	}
}

int main(int argc, char* argv[]) {
	std::fstream fin, fout;
	if(argc != 3) {
		std::cout << "Bad input.\n" << std::endl;
		return -1;
	}
	fin.open(argv[1], std::fstream::in);
	if(!fin) {
		std::cout << "Bad input.\n" << std::endl;
		return -1;
	}
	fout.open(argv[2], std::fstream::out);

	std::map<std::string, unsigned int> wordsWorkMap;
	std::multimap<unsigned int, std::string> wordsOutMap;
	unsigned int numbAllWords = 0;
	//scan file - fill map
	numbAllWords = analyseStr(fin, wordsWorkMap);
	
	//fill new map by reverse pairs
	fillWordsOutMap(wordsOutMap, wordsWorkMap);

	//print results
	for (auto i = wordsOutMap.rbegin(); i != wordsOutMap.rend(); ++i) {
		fout << i->second << ";" << i->first << ";" << (double)i->first/numbAllWords*100  << "%" << std::endl;
	}
	return 0;
}